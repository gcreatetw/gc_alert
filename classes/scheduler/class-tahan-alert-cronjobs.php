<?php
if (!defined('ABSPATH')) {
    exit;
}

class TAHAN_ALERT_CRON_JOB{
	
	public function __construct()
	{
		add_action( 'init', [ $this, 'tahan_update_cron_job' ] );
		
		add_action( 'tahan_run_update_cron_job', [ $this, "update_product_meta" ] );
		
		add_filter( 'cron_schedules', [ $this, "times" ], 10, 1 );
	}
	
	public function times( $schedules )
	{
		$schedules['every_six_hours'] = array(
			'interval' => 21600, // Every 6 hours
			'display'  => __( 'Every 6 hours' ),
		);
		return $schedules;
	}
	
	public function tahan_update_cron_job()
	{
		if ( ! wp_next_scheduled( 'tahan_run_update_cron_job' ) ) {
			wp_schedule_event( time(), 'every_six_hours', 'tahan_run_update_cron_job' );
		}
	}
	
	public function update_product_meta()
	{		
		$hash = self::generateHash(10);
		
		$log_url = TAHAN_LOG_DIR . '/product_meta_' . md5($hash) . date_i18n( 'Y-m' ) . '.log';
		
		try {
			
			$args = array(
				'post_type' => 'product',
				'post_status' => 'publish',
				'posts_per_page' => -1
			);

			/* Initialize WP_Query, and retrieve all product-pages */
			$loop = new WP_Query($args);

			/* Check if array contains any posts */
			if ($loop->have_posts()):
			
				while ($loop->have_posts()): 
				
					$loop->the_post();
					global $product;
					update_post_meta($product->get_id(), '_old_price', $product->get_regular_price(), $unique = false );
					
					$log = fopen( $log_url, 'a' );
					
					$array = [
						"id"=>$product->get_id(),
						"old_price"=>$product->get_regular_price(),
					];
					
					fwrite( $log, json_encode( $array, JSON_UNESCAPED_UNICODE ) );
					
					fclose( $log );
					
					$variations = $product->get_children();
					
					foreach( $variations as $variation ):
					
						$child_product = wc_get_product( $variation );
						
						$log = fopen( $log_url, 'a' );
						
						$array = [
							"id" => $variation,
							"old_price" => $child_product->get_regular_price(),
						];
				
						fwrite( $log, json_encode( $array, JSON_UNESCAPED_UNICODE ) );
						
						fclose( $log );
						
						update_post_meta( $variation, '_old_price', $child_product->get_regular_price(), $unique = false );
					endforeach;
				endwhile;
			endif;
			
		} catch (Exception $e) {
			
			gc_log( 'Caught exception: ' . $e->getMessage() . "\n", "CronJob" );
		}
	}
	
	static function generateHash($n = 7)
    {
        $key = '';
        $pattern = '123456789abcdefg';
        $counter = strlen($pattern) - 1;

        for ($i = 0; $i < $n; $i++) {

            $key .= $pattern[rand(0, $counter)];
        }

        return $key;
    }
}

return new TAHAN_ALERT_CRON_JOB();