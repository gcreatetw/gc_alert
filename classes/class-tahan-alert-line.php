<?php
if (!defined('ABSPATH')) {
    exit;
}

if( class_exists( "TAHAN_ALERT_SETTINGS" ) ){
	
	?>
		<form method="post" action="options.php">
		<?php 
			settings_fields( 'tahan_alert_line_setting' );
			do_settings_sections( 'tahan-line-notify-setting-admin' );
			submit_button();
		?>
		</form>
	<?php
}