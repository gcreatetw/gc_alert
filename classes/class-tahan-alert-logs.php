<?php
if (!defined('ABSPATH')) {
    exit;
}

if( class_exists( "TAHAN_ALERT_SETTINGS" ) ){
	
	settings_fields( 'tahan_alert_logs_setting' );
	do_settings_sections( 'tahan-logs-setting-admin' );
}
