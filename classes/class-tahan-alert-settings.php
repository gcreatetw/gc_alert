<?php
if (!defined('ABSPATH')) {
    exit;
}

class TAHAN_ALERT_SETTINGS{
	
	private static $instance = null;
	private $options;
	
    public static function forge()
	{
        if( is_null( self::$instance ) ){
			
            self::$instance = new self;
        }
        return self::$instance;
    }
	
	public function __construct()
	{
		add_action( 'admin_menu', [ $this, 'tahan_alert_plugin_setup_menu' ], 10 );
		
        add_action( 'admin_init', [ $this, 'page_init' ] );
		
        add_action( 'admin_footer', [ $this, 'tahan_admin_enqueue' ] );
	}
	
	public function tahan_admin_enqueue()
	{
		wp_register_style( 's2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' );
		wp_enqueue_style('s2');

		wp_register_script( 's2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', null, null, true );
		wp_enqueue_script('s2');
		
		wp_enqueue_style( 'admin-settings', plugins_url( '/assets/css/admin-panel.css', dirname( __FILE__ ) ) );
		
		wp_enqueue_script( 'admin-settings', plugins_url( '/assets/js/admin-settings.js', dirname( __FILE__ ) ), ['jquery'], false, false );
	}
	 
	public function tahan_alert_plugin_setup_menu()
	{
		add_menu_page(
			'Tahan Alert', // 頁面標題
			'Tahan Alert', // 選項標題
			'manage_options', //這個選單給用戶的功能
			'tahan_alert', // 選單的 slug 名稱（必須唯一）
			array( $this, 'love_wp_options_page_html' ), // 要輸出此頁面內容的函式
			'dashicons-superhero',
			//plugins_url( '/assets/images/icon.png', dirname(__FILE__) ), // 這個選單的的 icon 的URL
			1
		);
	}
	 
	public function love_wp_options_page_html(){
		
		if (!current_user_can('manage_options')) {
			
			return;
		}
		
		//Get the active tab from the $_GET param
		$default_tab = null;
		$tab = isset($_GET['tab']) ? $_GET['tab'] : $default_tab;
		
		
		?>
		 <!-- Our admin page content should all be inside .wrap -->
		<div class="wrap">
			<!-- Print the page title -->
			<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
			<!-- Here are our tabs -->
			<nav class="nav-tab-wrapper">
				<a href="?page=tahan_alert" class="nav-tab <?php if($tab===null):?>nav-tab-active<?php endif; ?>">General</a>
				<a href="?page=tahan_alert&tab=line" class="nav-tab <?php if($tab==='line'):?>nav-tab-active<?php endif; ?>">LINE</a>
				<a href="?page=tahan_alert&tab=woocommerce" class="nav-tab <?php if($tab==='woocommerce'):?>nav-tab-active<?php endif; ?>">WooCommerce</a>
				<a href="?page=tahan_alert&tab=logs" class="nav-tab <?php if($tab==='logs'):?>nav-tab-active<?php endif; ?>">Logs</a>
			</nav>

			<div class="tab-content">
			<?php switch($tab) :
			  case 'line':
			  
				include_once dirname( TAHAN_ALERT_PLUGIN_FILE ) . '/classes/class-tahan-alert-line.php';
				break;
			  case 'woocommerce':
			  
				include_once dirname( TAHAN_ALERT_PLUGIN_FILE ) . '/classes/class-tahan-alert-wc.php';
				break;
			  case 'logs':
			  
				include_once dirname( TAHAN_ALERT_PLUGIN_FILE ) . '/classes/class-tahan-alert-logs.php';
				break;
			  default:
			  
				include_once dirname( TAHAN_ALERT_PLUGIN_FILE ) . '/classes/class-tahan-alert-general.php';
				break;
			endswitch; ?>
			</div>
		</div>
        <?php
	}
	
	public function page_init()
	{
		/*
		 * General Settings
		 */
		register_setting(
            'tahan_alert_general_setting', // Option group
            'tahan_alert_general', // Option name
            array( $this, 'sanitize_general' ) // Sanitize
        );

        add_settings_section(
            'general_setting_section', // ID
            'General Settings', // Title
            array( $this, 'general_settings' ), // Callback
            'tahan-general-setting-admin' // Page
        );
		
		/*
		 * LINE Settings
		 */		
		register_setting(
            'tahan_alert_line_setting', // Option group
            'tahan_alert_line', // Option name
            array( $this, 'sanitize_line' ) // Sanitize
        );

        add_settings_section(
            'line_setting_section', // ID
            'LINE Notify Settings', // Title
            array( $this, 'line_notify' ), // Callback
            'tahan-line-notify-setting-admin' // Page
        );
		
		/*
		 * Logs Settings
		 */		
		register_setting(
            'tahan_alert_logs_setting', // Option group
            'tahan_alert_logs', // Option name
            array( $this, 'sanitize_log' ) // Sanitize
        );

        add_settings_section(
            'logs_setting_section', // ID
            'Logs View', // Title
            array( $this, 'logs' ), // Callback
            'tahan-logs-setting-admin' // Page
        );
		
		/*
		 * WooCommerce Settings
		 */	
		if (is_plugin_active('woocommerce/woocommerce.php')) {
			
			register_setting(
				'tahan_alert_wc_setting', // Option group
				'tahan_alert_wc', // Option name
				array( $this, 'sanitize_wc' ) // Sanitize
			);

			add_settings_section(
				'wc_setting_section', // ID
				'Woocommerce Settings', // Title
				array( $this, 'woocommerce' ), // Callback
				'tahan-wc-setting-admin' // Page
			);
		}
	}
	
	public function sanitize_general( $input )
    {
		if ( ! is_admin() ) {
			
			return;
		}
        $new_input = array();
		if( isset( $input['role'] ) && is_array ( $input['role'] ) ){
            foreach( $input['role'] as $key => $role  ){
				$new_input['role'][$key] = sanitize_text_field( $role );
			}
		}
		
        return $new_input;
    }
	
	public function sanitize_line( $input )
    {
		if ( ! is_admin() ) {
			
			return;
		}
        $new_input = array();
        
        if( isset( $input['line_active_checkbox'] ) )
            $new_input['line_active_checkbox'] = absint( $input['line_active_checkbox'] );
		
        if( isset( $input['line_active_key'] ) )
            $new_input['line_active_key'] = sanitize_text_field( $input['line_active_key'] );
		
        return $new_input;
    }
	
	public function sanitize_wc( $input )
    {
		if ( ! is_admin() ) {
			
			return;
		}
        $new_input = array();
		
        if( isset( $input['wc_price_adjustment_range'] ) )
            $new_input['wc_price_adjustment_range'] = absint( $input['wc_price_adjustment_range'] );
		
		if( isset( $input['categories'] ) && is_array ( $input['categories'] ) ){
            foreach( $input['categories'] as $key => $category  ){
				$new_input['categories'][$key] = absint( $category );
			}
		}
		
        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function general_settings( /* $section_passed */ )
    {
        add_settings_field(
            'user_role_selected', // ID
            __( 'Select user role', 'gc_alert' ), // Title 
            array( $this, 'user_role' ), // Callback
            'tahan-general-setting-admin', // Page
            'general_setting_section' // Section           
        );
	}

    /** 
     * Print the Section text
     */
    public function line_notify( /* $section_passed */ )
    {
        add_settings_field(
            'line_active_checkbox', // ID
            __( 'enable/disable', 'gc_alert' ), // Title 
            array( $this, 'line_checkbox' ), // Callback
            'tahan-line-notify-setting-admin', // Page
            'line_setting_section' // Section           
        );

        add_settings_field(
            'line_active_key', // ID
            __( 'Token key', 'gc_alert' ), // Title 
            array( $this, 'line_key' ), // Callback
            'tahan-line-notify-setting-admin', // Page
            'line_setting_section' // Section           
        );
	}
	
    public function woocommerce()
    {
        add_settings_field(
            'price_adjustment', // ID
            __( 'Price Range', 'gc_alert' ), // Title 
            array( $this, 'price_adjustment' ), // Callback
            'tahan-wc-setting-admin', // Page
            'wc_setting_section' // Section           
        );
		
		add_settings_field(
			'wc_categories', // ID
			__( 'Select Category', 'gc_alert' ), // Title 
			array( $this, 'select_category' ), // Callback
			'tahan-wc-setting-admin', // Page
			'wc_setting_section' // Section     
		);
    }

    public function user_role()
    {
		$this->options = get_option( 'tahan_alert_general' );
				
		//取得角色列表
		global $wp_roles;

		if ( ! isset( $wp_roles ) )
			$wp_roles = new WP_Roles();
		
		echo '<select class="role_select" multiple="multiple" name="tahan_alert_general[role][]">';
		foreach( $wp_roles->roles as $k => $all_r ){
			$selected = "";
			if( in_array( $k, $this->options['role'] ) ){
				$selected = "selected";
			}
			echo "<option value='" . $k . "' ".$selected.">" . $k . "</option>";
		}
		echo '</select>';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function line_checkbox()
    {
		$this->options = get_option( 'tahan_alert_line' );
        printf(
            '<input type="checkbox" id="line_active_checkbox" name="tahan_alert_line[line_active_checkbox]" value="1" %s/>',
			checked( $this->options['line_active_checkbox'], '1', false )
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function line_key()
    {
		$this->options = get_option( 'tahan_alert_line' );
		printf(
            '<input type="text" id="line_active_key" name="tahan_alert_line[line_active_key]" value="%s" />',
            isset( $this->options['line_active_key'] ) ? esc_attr( $this->options['line_active_key']) : ''
        );
    }
	
	

    /* 
     * WooCommerce Setings
     */
    public function price_adjustment()
    {
		$this->options = get_option( 'tahan_alert_wc' );
        printf(
            '<input type="text" id="wc_price_adjustment_range" name="tahan_alert_wc[wc_price_adjustment_range]" value="%s" />',
			isset( $this->options['wc_price_adjustment_range'] ) ? esc_attr( $this->options['wc_price_adjustment_range']) : ''
        );
    }
	
    public function select_category()
    {
		$this->options = get_option( 'tahan_alert_wc' );
		$taxonomy     = 'product_cat';
		$orderby      = 'menu_order';  
		$show_count   = 0;      // 1 for yes, 0 for no
		$pad_counts   = 0;      // 1 for yes, 0 for no
		$hierarchical = 1;      // 1 for yes, 0 for no  
		$title        = '';  
		$empty        = 0;
		
		$args = array(
			'taxonomy'     => $taxonomy,
			'orderby'      => $orderby,
			'show_count'   => $show_count,
			'pad_counts'   => $pad_counts,
			'hierarchical' => $hierarchical,
			'title_li'     => $title,
			'hide_empty'   => $empty
		);
		$all_categories = get_categories( $args );
		
		echo '<select class="category_select" multiple="multiple" name="tahan_alert_wc[categories][]">';
		foreach( $all_categories as $all_c ){
			$selected = "";
			if( in_array( $all_c->term_id, $this->options['categories'] ) ){
				$selected = "selected";
			}
			echo "<option value='" . $all_c->term_id . "' ".$selected.">" . $all_c->name . "</option>";
		}
		echo '</select>';
    }
	
	public function logs()
	{
		$logs = self::get_log_files();
		if ( ! empty( $_REQUEST['log_file'] ) && isset( $logs[ sanitize_title( wp_unslash( $_REQUEST['log_file'] ) ) ] ) ) { // WPCS: input var ok, CSRF ok.
			$viewed_log = $logs[ sanitize_title( wp_unslash( $_REQUEST['log_file'] ) ) ]; // WPCS: input var ok, CSRF ok.
		} elseif ( ! empty( $logs ) ) {
			$viewed_log = current( $logs );
		}
		$handle = ! empty( $viewed_log ) ? self::get_log_file_handle( $viewed_log ) : '';
		
		include_once __DIR__ . '/views/html-logs.php';
		
		
		/* $log_url = TAHAN_LOG_DIR . '/product_alert_' . date_i18n( 'Y-m' ) . '.json';
		
		if( file_exists( $log_url ) ) :
		
		echo "<div class='tahan_log'><pre>" .  esc_html( file_get_contents( $log_url ) ) . "</pre></div>";
		
		
		else :
		
			esc_html_e( "Unable to open file!" );
			
		endif; */
	}
	
	public static function get_log_files() {
		$files  = @scandir( TAHAN_LOG_DIR ); // @codingStandardsIgnoreLine.
		$result = array();

		if ( ! empty( $files ) ) {
			foreach ( $files as $key => $value ) {
				if ( ! in_array( $value, array( '.', '..' ), true ) ) {
					if ( ! is_dir( $value ) && strstr( $value, '.log' ) ) {
						$result[ sanitize_title( $value ) ] = $value;
					}
				}
			}
		}

		return $result;
	}
	public static function get_log_file_handle( $filename ) {
		return substr( $filename, 0, strlen( $filename ) > 48 ? strlen( $filename ) - 48 : strlen( $filename ) - 4 );
	}
}