<?php
if (!defined('ABSPATH')) {
    exit;
}

if( !class_exists( "TAHAN_ALERT_SETTINGS" ) ){
	return;
}

class TAHAN_ALERT_VALID
{	
	public $options;
		
	public function __construct()
	{
		add_action( 'save_post', [ $this, 'admin_process_simple_product' ], 999, 1);
		
		//add_action( 'save_post', [ $this, 'admin_process_variable_product' ], 999, 1 );
		
		add_action( 'woocommerce_process_product_meta_variable', [ $this, 'admin_process_variable_product' ], 999, 1 );
		
		add_filter( 'post_row_actions', [ $this, 'my_disable_quick_edit' ], 10, 2 );
		
		add_filter( 'bulk_actions-edit-product',  [ $this, 'remove_from_bulk_actions' ], 999, 1 );
	}
	
	public function remove_from_bulk_actions( $actions )
	{
		unset( $actions[ 'edit' ] );
        return $actions;
	}
	
	public function my_disable_quick_edit( $actions = array(), $post = null )
	{
		unset($actions['inline hide-if-no-js']);
		return $actions;
	}
	
	public function admin_process_simple_product( $product )
	{
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
			return;
		
		$products = wc_get_product( $product );
		$this->options = get_option( 'tahan_alert_wc' );
		$alert = false;
		if ( $products ):
			
			$product_meta_data 		= get_post_meta( $product, "_old_price", true );
			$range 					= $this->options['wc_price_adjustment_range'];
			
			$new_regular_price 		= $products->regular_price;
			//$changes 				= $products->get_changes();
			$sku 					= $products->get_sku();
			
			$price_array = [];
			//判斷是否有資料可比對
			
			$price = [ '_price' => $new_regular_price ];
			
			
			if( isset( $product_meta_data ) ):
				
				if( $product_meta_data['_price'] > $price['_price'] ):				
									
					$price_range = ( ( $price['_price'] / $product_meta_data['_price'] ) * 100 );
					
					if( $price_range < $range ):
						
						$alert = true;
						$data = [
							'time'  		 => date("Y-m-d H:i:sa"),
							'id' 			 => $product,
							'original_price' => $product_meta_data['_price'],
							'new_price' 	 => $price['_price'],
							'price_range' 	 => ( 100 - $price_range )
						];
						
						$this->generate_log( $product, $data );
						$this->line_notify( $sku, $data );
						
						/* remove_action( 'save_post', [ $this, 'admin_process_simple_product' ], 999, 1);
												
						wp_update_post( [ 'ID' => $product, 'post_status' =>  'pending' ] );
						
						add_action( 'save_post', [ $this, 'admin_process_simple_product' ], 999, 1); */
						
					endif;
				endif;				
			else:
				
				//暫定第一次更新的商品無任何執行需求
			endif;
						
			update_post_meta( $product, "_old_price", $price, $unique = false );
			// Update product cache
			wc_delete_product_transients(  $product );
			
			if( $alert == true):
				
				remove_action( 'save_post', [ $this, 'admin_process_simple_product' ], 999, 1);
				
				wp_update_post( [ 'ID' => $product, 'post_status' =>  'pending' ] );
				
				add_action( 'save_post', [ $this, 'admin_process_simple_product' ], 999, 1);
			endif;
		endif;
	}
	/* 
	public function admin_process_variable_product( $post_id )
	{		
		$this->options 	= get_option( 'tahan_alert_wc' );
		$range 			= $this->options['wc_price_adjustment_range'];
		$product 		= wc_get_product( $post_id );
		$variations 	= $product->get_children();
		$alert 			= false;
		
		foreach( $variations as $variation ){
			
			$child_product		= wc_get_product( $variation );
			$product_meta_data 	= get_post_meta( $variation, "_old_price", true );
			
			$new_regular_price 	= $child_product->get_regular_price();
			$sku			 	= $child_product->get_sku();
			$price 				= [ "_price" => $new_regular_price ];
			
			//判斷是否有資料可比對
			if( isset( $product_meta_data ) ):
				
				if( $product_meta_data['_price'] > $price['_price'] ):
					
					$price_range = ( ( $price['_price']/ $product_meta_data['_price'] ) * 100 );
					
					if( $price_range < $range ):
					
						$alert 	= true;
						$data 	= [
							'time' => date("Y-m-d H:i:sa"),
							'id'=> $variation,
							'original_price'=> $product_meta_data['_price'],
							'new_price'=> $price['_price'],
							'price_range'=> ( 100 - $price_range )
						];
						$this->generate_log( $variation, $data );
						//$this->line_notify( $sku, $data );
					endif;
				endif;
			endif;
			
			update_post_meta( $variation, "_old_price", $price, $unique = false );
		}
		if( $alert == true):
			
			wp_update_post( [ 'ID' => $post_id, 'post_status' =>  'pending' ] );
		endif;
	} */
	
	public function admin_process_variable_product( $post_id )
	{		
		$this->options 	= get_option( 'tahan_alert_wc' );
		$range 			= $this->options['wc_price_adjustment_range'];
		$product 		= wc_get_product( $post_id );
		$variations 	= $product->get_children();
		$alert 			= false;
		
		foreach( $variations as $variation ){
			
			$child_product		= wc_get_product( $variation );
			$product_meta_data 	= get_post_meta( $variation, "_old_price", true );
			
			$new_regular_price 	= $child_product->get_regular_price();
			$sku			 	= $child_product->get_sku();
			$price 				= [ "_price" => $new_regular_price ];
			
			//判斷是否有資料可比對
			if( isset( $product_meta_data ) ):
				
				if( $product_meta_data['_price'] > $price['_price'] ):
					
					$price_range = ( ( $price['_price']/ $product_meta_data['_price'] ) * 100 );
					
					if( $price_range < $range ):
					
						$alert 	= true;
						$data 	= [
							'time' => date("Y-m-d H:i:sa"),
							'id'=> $variation,
							'original_price'=> $product_meta_data['_price'],
							'new_price'=> $price['_price'],
							'price_range'=> ( 100 - $price_range )
						];
						$this->generate_log( $variation, $data );
						$this->line_notify( $sku, $data );
					endif;
				endif;
			endif;
			
			update_post_meta( $variation, "_old_price", $price, $unique = false );
		}
		if( $alert == true):
			
			add_action( 'save_post', [ $this, 'admin_process_variable_product' ], 999, 1 );
			
			wp_update_post( [ 'ID' => $post_id, 'post_status' =>  'pending' ] );
			
			remove_action( 'save_post', [ $this, 'admin_process_variable_product' ], 999, 1 );
		endif;
	}
	
	public function generate_log( $id, $data )
	{			
		$log_url = TAHAN_LOG_DIR . '/product_alert_' . date_i18n( 'Y-m' ) . '.log';
		
		$user = wp_get_current_user();
		
		$mail = $user->user_email;
			
		$log = fopen( $log_url, 'a' );
		
		/***/
		$log_data  = "時間: " . date_i18n("Y-m-d H:i:s") . PHP_EOL .
		"商品編號: " . $data['id'] . PHP_EOL .
		"舊價格: " . $data['original_price'] . PHP_EOL .
		"新價格: " . $data['new_price'] . PHP_EOL .
		"售價價差(%): " . $data['price_range'] . PHP_EOL .
		"管理員信箱: ".$mail . PHP_EOL .
		"__________________________________________" . PHP_EOL;
		/***/
		
		fwrite( $log, $log_data );
		
		fclose( $log );

	}
	
	public function line_notify( $id, $data )
	{
		$this->options 	= get_option( 'tahan_alert_line' );
		$user 			= wp_get_current_user();
		$curl 			= curl_init();
		$token			= $this->options['line_active_key'];
		$message 		= sprintf( '商品貨號：【%s】，由帳號：【%s】 調整售價價差：【%s】％', $id, $user->user_email, $data['price_range'] );
				
		if( empty( $token ) ){
			
			$token = 'jMK8ofdYTJgLtGm1dfrSzKaJFXrmR14h3UDTVfnzWSc'; //demo LINE Notify token
		}
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://notify-api.line.me/api/notify",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query( ['message' => $message ] ),
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $token,
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
	}
}

return new TAHAN_ALERT_VALID();