<?php
if (!defined('ABSPATH')) {
    exit;
}

if( class_exists( "TAHAN_ALERT_SETTINGS" ) ){
	
	?>
		<form method="post" action="options.php">
		<?php 
			settings_fields( 'tahan_alert_general_setting' );
			do_settings_sections( 'tahan-general-setting-admin' );
			submit_button();
		?>
		</form>
	<?php
}