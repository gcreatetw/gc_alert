<?php
if (!defined('ABSPATH')) {
    exit;
}

if( !class_exists( "TAHAN_ALERT_SETTINGS" ) ){
	return;
}

class TAHAN_ALERT_LOG {
	
	public function __construct()
	{
		$this->create_file();
	}
	
	function create_file()
	{
		if( ! file_exists( TAHAN_LOG_DIR ) ) {
			
			mkdir(TAHAN_LOG_DIR, 0755);
			$htaccess_file = fopen( TAHAN_LOG_DIR . '/.htaccess', 'w' );
			fwrite( $htaccess_file, "<FILES *>\ndeny from All\n</FILES>" );
			fclose($htaccess_file);
		}
	}
}

return new TAHAN_ALERT_LOG();