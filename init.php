<?php
/*
 * Plugin Name:  GCreate Alert
 * Plugin URI:   https://www.gcreate.com.tw
 * Description:  Alert Administrator when product price adjustment range lower 10%. 
 * Version:      1.1
 * Author:       GCreate
 * Author URI:   https://www.gcreate.com.tw
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
*/

if( !defined('ABSPATH')){
	
    exit;
}


add_action( 'plugins_loaded', 'init_alert', 100 );
function init_alert()
{	
	define_path();
	include_once dirname( TAHAN_ALERT_PLUGIN_FILE ) . '/classes/class-tahan-alert-settings.php';
	include_once dirname( TAHAN_ALERT_PLUGIN_FILE ) . '/classes/class-tahan-alert-validation.php';
	include_once dirname( TAHAN_ALERT_PLUGIN_FILE ) . '/classes/class-tahan-alert-log-dir.php';
	include_once dirname( TAHAN_ALERT_PLUGIN_FILE ) . '/classes/scheduler/class-tahan-alert-cronjobs.php';
	TAHAN_ALERT_SETTINGS::forge();
	//WC_ALERT_SETTINGS::forge();
}

add_action( 'init', 'gc_alert_load_textdomain', 100 );
function gc_alert_load_textdomain() {
	
	load_plugin_textdomain( 'gc_alert', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' ); 
}

function define_path()
{
	$upload_dir = wp_upload_dir( null, false );
	
	if ( ! defined( 'TAHAN_ALERT_PLUGIN_FILE' ) ) {
		
		define( 'TAHAN_ALERT_PLUGIN_FILE', __FILE__ );
	}
	define( 'TAHAN_LOG_DIR', $upload_dir['basedir'] . '/tahan-logs/' );
	
	
}